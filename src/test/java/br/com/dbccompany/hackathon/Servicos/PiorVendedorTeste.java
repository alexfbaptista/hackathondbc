package br.com.dbccompany.hackathon.Servicos;

import br.com.dbccompany.hackathon.Model.VendaEValorTotalModel;
import br.com.dbccompany.hackathon.Model.VendasModel;
import br.com.dbccompany.hackathon.Model.VendedorModel;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PiorVendedorTeste {

    @Test
    public void retornaPiorVendedor() {
        List<VendasModel> vendas = new ArrayList<>();

        VendasModel venda1 =
                new VendasModel("10",
                        "[1-10-100,2-500-250,3-40-3.10]", "Maria");
        VendasModel venda2 =
                new VendasModel("08",
                        "[1-34-10,2-33-1.50,3-40-0.10]", "Paulo");
        VendasModel venda3 =
                new VendasModel("05",
                        "[1-34-0.5,2-33-0.20,3-40-0.10]", "Fernando");

        vendas.add(venda1);
        vendas.add(venda2);
        vendas.add(venda3);

        VendaEValorTotalModel piorVendedor = new PiorVendedor().run(vendas);

        assertEquals("05", piorVendedor.getId());
        assertEquals("Fernando", piorVendedor.getNomeVendedor());

    }
}

package br.com.dbccompany.hackathon.Servicos;

import br.com.dbccompany.hackathon.Model.ItemModel;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculaValorTotalPorVendasTeste {

    @Test
    public void retornarValorTotal() {
        ItemModel item1 = new ItemModel("01", 50, 50.0);
        ItemModel item2 = new ItemModel("01", 50, 50.0);
        ItemModel item3 = new ItemModel("01", 50, 50.0);

        List<ItemModel> items = new ArrayList<>();
        items.add(item1);
        items.add(item2);
        items.add(item3);

        Double total = new CalculaValorTotalPorVendas().run(items);

        assertEquals(7500.0, total);

    }
}

package br.com.dbccompany.hackathon.Builder;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ComponenteBuilderTeste {

    private String data = "001ç1234567891234çPedroç50000";

    @Test
    public void retornaComponentesDivididosEmÇ() {
        ComponenteBuilder builder = new ComponenteBuilder();
        String[] componentes = builder.run(data);
        assertEquals("001", componentes[0]);
        assertEquals("1234567891234", componentes[1]);
        assertEquals("Pedro", componentes[2]);
        assertEquals("50000", componentes[3]);
    }
}

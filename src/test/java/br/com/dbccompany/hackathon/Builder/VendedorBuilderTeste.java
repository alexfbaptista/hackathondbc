package br.com.dbccompany.hackathon.Builder;

import br.com.dbccompany.hackathon.Model.VendedorModel;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VendedorBuilderTeste {

    private String[] data = {"001", "1234567891234", "Pedro", "50000"};

    @Test
    public void retornaVendedorModel() {
        VendedorBuilder builder = new VendedorBuilder();
        VendedorModel vendedor = builder.run(data);
        assertEquals("1234567891234", vendedor.getCpf());
        assertEquals("Pedro", vendedor.getNome());
        assertEquals(50000.0, vendedor.getSalario());
    }
}

package br.com.dbccompany.hackathon.Builder;

import br.com.dbccompany.hackathon.Model.VendasModel;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class VendasBuilderTeste {

    String[] data = {"003", "10" , "[1-10-100,2-30-2.50,3-40-3.10]", "Pedro"};

    @Test
    public void retornarUmVendasModel() {
        VendasBuilder builder = new VendasBuilder();
        VendasModel vendas = builder.run(data);
        assertEquals("10", vendas.getId());
        assertEquals("[1-10-100,2-30-2.50,3-40-3.10]", vendas.getItens());
        assertEquals("Pedro", vendas.getNome());
    }
}

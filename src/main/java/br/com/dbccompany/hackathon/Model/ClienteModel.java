package br.com.dbccompany.hackathon.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClienteModel {

    private String cnpj;
    private String nome;
    private String areaAtuacao;

}

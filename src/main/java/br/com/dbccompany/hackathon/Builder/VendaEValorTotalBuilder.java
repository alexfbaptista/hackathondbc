package br.com.dbccompany.hackathon.Builder;

import br.com.dbccompany.hackathon.Model.ItemModel;
import br.com.dbccompany.hackathon.Model.VendaEValorTotalModel;
import br.com.dbccompany.hackathon.Model.VendasModel;
import br.com.dbccompany.hackathon.Servicos.CalculaValorTotalPorVendas;

import java.util.List;

public class VendaEValorTotalBuilder {
    public VendaEValorTotalModel run(VendasModel vendas) {
        ItemBuilder builder = new ItemBuilder();
        List<ItemModel> items = builder.run(vendas.getItens());
        VendaEValorTotalModel vendaEValorTotal = new VendaEValorTotalModel();
        vendaEValorTotal.setTotal(new CalculaValorTotalPorVendas().run(items));
        vendaEValorTotal.setId(vendas.getId());
        vendaEValorTotal.setNomeVendedor(vendas.getNome());
        return vendaEValorTotal;
    }
}

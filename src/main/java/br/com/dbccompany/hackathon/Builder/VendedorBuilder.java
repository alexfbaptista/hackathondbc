package br.com.dbccompany.hackathon.Builder;

import br.com.dbccompany.hackathon.Model.VendedorModel;

public class VendedorBuilder {
    public VendedorModel run(String[] s) {
        return new VendedorModel(s[1], s[2], Double.parseDouble(s[3]));
    }
}

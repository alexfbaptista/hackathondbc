package br.com.dbccompany.hackathon.Servicos;

import br.com.dbccompany.hackathon.Builder.VendaEValorTotalBuilder;
import br.com.dbccompany.hackathon.Model.VendaEValorTotalModel;
import br.com.dbccompany.hackathon.Model.VendasModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class VendaMaisCara {

    private List<VendaEValorTotalModel> vendasEValores;

    {
        vendasEValores = new ArrayList<>();
    }

    public VendaEValorTotalModel run(List<VendasModel> vendas) {
        if (vendas.isEmpty()) {
            return null;
        }
        populaListaDeVendaESeusValores(vendas);
        Collections.sort(vendasEValores, new Comparator<VendaEValorTotalModel>() {
            @Override
            public int compare(VendaEValorTotalModel o1, VendaEValorTotalModel o2) {
                return Double.compare(o1.getTotal(), o2.getTotal());
            }
        });
        return vendasEValores.get(vendasEValores.size() - 1);
    }

    public void populaListaDeVendaESeusValores(List<VendasModel> vendas) {
        VendaEValorTotalBuilder vendaEValorTotalBuilder = new VendaEValorTotalBuilder();
        for (VendasModel v : vendas) {
            vendasEValores.add(vendaEValorTotalBuilder.run(v));
        }
    }
}

package br.com.dbccompany.hackathon.Servicos;

import br.com.dbccompany.hackathon.Model.ItemModel;

import java.util.List;
import java.util.stream.DoubleStream;

public class CalculaValorTotalPorVendas {
    public Double run(List<ItemModel> items) {
        return items
                .stream()
                .flatMapToDouble(s ->
                        DoubleStream.of(s.getValor() * s.getQuantidade()))
                .sum();
    }
}

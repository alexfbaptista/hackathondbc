package br.com.dbccompany.hackathon.Servicos;

import br.com.dbccompany.hackathon.Builder.ComponenteBuilder;

public class ContadorDeVendedoresEClientes {

    private int quantidadeClientes;
    private int quantidadeVendedores;

    {
        quantidadeClientes = 0;
        quantidadeVendedores = 0;
    }

    public void run(String[] data) {
        for (int i = 0; i < data.length; i++) {
            String[] componentes = new ComponenteBuilder().run(data[i]);
            switch (componentes[0]) {
                case "001":
                    quantidadeVendedores++;
                    break;
                case "002":
                    quantidadeClientes++;
                    break;
            }
        }
    }

    public int getQuantidadeClientes() {
        return quantidadeClientes;
    }

    public void setQuantidadeClientes(int quantidadeClientes) {
        this.quantidadeClientes = quantidadeClientes;
    }

    public int getQuantidadeVendedores() {
        return quantidadeVendedores;
    }

    public void setQuantidadeVendedores(int quantidadeVendedores) {
        this.quantidadeVendedores = quantidadeVendedores;
    }
}

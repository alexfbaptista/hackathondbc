package br.com.dbccompany.hackathon.Repository;

import br.com.dbccompany.hackathon.Entity.ConsultorEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConsultorRepository extends CrudRepository<ConsultorEntity, String> {

    Optional<ConsultorEntity> findByLogin(String login);
}

package br.com.dbccompany.hackathon.Repository;

import br.com.dbccompany.hackathon.Entity.RelatorioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RelatorioRepository extends CrudRepository<RelatorioEntity, Integer> {

}

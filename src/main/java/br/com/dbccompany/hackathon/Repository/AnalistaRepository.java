package br.com.dbccompany.hackathon.Repository;

import br.com.dbccompany.hackathon.Entity.AnalistaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnalistaRepository  extends CrudRepository<AnalistaEntity, String> {

    AnalistaEntity findByLogin(String login);
}

package br.com.dbccompany.hackathon.Monitoramento;

import br.com.dbccompany.hackathon.MultilineExample.MultLineJob;
import br.com.dbccompany.hackathon.URI.URI;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Map;


import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;

@EnableBatchProcessing
@Component
public class MonitoramentoDiretorio {

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    Job job;

    @Autowired
    private MultLineJob utilizationBatchConfiguration;


    @PostConstruct
    @Async
    public void run(){
        try {
            WatchService watcher = FileSystems.getDefault().newWatchService();
            Path diretorio = Paths.get(URI.INPUT);
            diretorio.register(watcher, ENTRY_CREATE);

            ChecaExtensaoArquivo extensaoArquivo = new ChecaExtensaoArquivo();

            System.out.println("Sistema de monitoramento de diretorio iniciado em: "
                    + URI.INPUT);

            while (true) {
                WatchKey key;
                try {
                    key = watcher.take();
                } catch (InterruptedException ex) {
                    return;
                }

                for (WatchEvent<?> eventosNoDiretorio : key.pollEvents()) {
                    WatchEvent.Kind<?> tipoDeEvento = eventosNoDiretorio.kind();
                    WatchEvent<Path> evento = (WatchEvent<Path>) eventosNoDiretorio;
                    if (tipoDeEvento.equals(ENTRY_CREATE)) {
                        if (extensaoArquivo.isArquivoDat(evento.context())) {

                            //impoerJob(); Erro de execuçao -> Table "BATCH_JOB_INSTANCE" not found

                            System.out.println("...ENTROU ARQUIVO DAT E PODE SER PROCESSADO");
                        } else {
                            //LOG DE ARQUIVO NAO .DAT INSERIDO
                            System.out.println(".. ENTROU ARQUIVO NAO DAT E NAO PODE SER PROCESSADO");
                        }
                    }
                }

                boolean valid = key.reset();
                if (!valid) {
                    break;
                }
            }

        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    private void impoerJob() {

        Map<String, JobParameter> confMap = new HashMap<String, JobParameter>();
        confMap.put("time", new JobParameter(System.currentTimeMillis()));
        JobParameters jobParameters = new JobParameters(confMap);
        try {
            JobExecution ex = jobLauncher.run(utilizationBatchConfiguration.job(), jobParameters);
        } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException
                | JobParametersInvalidException e) {
            e.printStackTrace();
        }
    }
}

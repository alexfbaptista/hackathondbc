package br.com.dbccompany.hackathon.Entity;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
public class RelatorioEntity {

    @Id
    @SequenceGenerator(name = "RELATORIO_SEQ", sequenceName = "RELATORIO_SEQ")
    @GeneratedValue(generator = "RELATORIO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private int countClientes;

    private int countVendedores;

    private String idVendaMaisCara;

    private String nomePiorVendedor;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCountClientes() {
        return countClientes;
    }

    public void setCountClientes(int countClientes) {
        this.countClientes = countClientes;
    }

    public int getCountVendedores() {
        return countVendedores;
    }

    public void setCountVendedores(int countVendedores) {
        this.countVendedores = countVendedores;
    }

    public String getIdVendaMaisCara() {
        return idVendaMaisCara;
    }

    public void setIdVendaMaisCara(String idVendaMaisCara) {
        this.idVendaMaisCara = idVendaMaisCara;
    }

    public String getNomePiorVendedor() {
        return nomePiorVendedor;
    }

    public void setNomePiorVendedor(String nomePiorVendedor) {
        this.nomePiorVendedor = nomePiorVendedor;
    }
}
